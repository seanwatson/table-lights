from threading import Lock

import paho.mqtt.client as mqtt
from RPi import GPIO

from pyparts.parts.led import rgb_led
from pyparts.platforms import raspberrypi_platform as rpi_platform

_RED_PIN = 5
_GREEN_PIN = 6
_BLUE_PIN = 13

_PIN_NAMING_SCHEME = rpi_platform.BCM
_PWM_FREQUENCY_HZ = 2000

_MQTT_USERNAME = 'sean'
_MQTT_PASSWORD = '<redacted>'
_MQTT_HOST = 'mosquitto.seanwatson.io'
_MQTT_CLIENT_ID = 'tablelights'
_MQTT_TLS_CA_FILE = '/home/pi/certs.pem'
_MQTT_TOPIC_ROOT = 'homeautomation/lights/tablelight/'


class LightServer(object):

  def __init__(self, rgb):
    """Constructs a LightServer object.

    Args:
      rgb: A pyparts.parts.led.RGBLed object.
    """
    self._rgb = rgb
    self._lock = Lock()
    self._client = mqtt.Client(client_id=_MQTT_CLIENT_ID)
    self._client.tls_set(_MQTT_TLS_CA_FILE)
    self._client.tls_insecure_set(False)
    self._client.username_pw_set(_MQTT_USERNAME, _MQTT_PASSWORD)
    self._client.on_connect = self.on_connect
    self._client.on_message = self.on_message
    self._started = False

  def __del__(self):
    """Deconstructor. Cleans up GPIO pins."""
    GPIO.cleanup()

  def on_connect(self, client, userdata, flags, rc):
    """Callback for when MQTT connects to the server.

    Args:
      client: MQTT client object.
      userdata: Private userdata set at client creation time.
      flags: Response flags.
      rc: Connection result.
    """
    client.subscribe(_MQTT_TOPIC_ROOT + '#')

  def on_message(self, client, userdata, msg):
    """Callback for when MQTT receives a message.

    Args:
      client: MQTT client object.
      userdata: Private userdata set at client creation time.
      msg: The topic and payload that was published.
    """
    try:
      if msg.topic == _MQTT_TOPIC_ROOT + 'status/switch':
        if msg.payload == 'ON':
          self.enable()
        elif msg.payload == 'OFF':
          self.disable()
      elif msg.topic == _MQTT_TOPIC_ROOT + 'brightness/set':
        brightness = int(int(msg.payload) / 255.0)
        self.set_rgb(self.get_red() * brightness,
                     self.get_green() * brightness,
                     self.get_blue() * brightness)
        client.publish(_MQTT_TOPIC_ROOT + 'brightness/status',
                       payload=msg.payload,
                       qos=2,
                       retain=True)
      elif msg.topic == _MQTT_TOPIC_ROOT + 'rgb/set':
        vals = msg.payload.split(',')
        red = int(int(vals[0]) / 255.0 * 100)
        green = int(int(vals[1]) / 255.0 * 100)
        blue = int(int(vals[2]) / 255.0 * 100)
        self.set_rgb(red, green, blue)
    except Exception as e:
      pass

  def _publish_message(topic, payload):
    if not self._started:
      return
    self._client.publish(_MQTT_TOPIC_ROOT + topic,
                         payload=payload,
                         qos=2,
                         retain=True)

  def set_pwm_frequency(self, frequency_hz):
    """Sets the frequency of the PWM pins.
    
    Args:
      frequency_hz: The frequency to set the PWM pins to.
    """
    self._lock.acquire()
    self._rgb.set_pwm_frequency(frequency_hz)
    self._lock.release()

  def get_pwm_frequency(self):
    """Gets the frequency of the PWM pins.
    
    Returns:
      The frequency of the PWM pins as an integer.
    """
    self._lock.acquire()
    self._rgb.get_pwm_frequency()
    self._lock.release()

  def set_rgb(self, red, green, blue):
    """Set the red, green, and blue values.

    Args:
      red: Integer, 0-100. Value for red led.
      green: Integer, 0-100. Value for green led.
      blue: Integer, 0-100. Value for blue led.
    """
    if (red < 0 or red > 100 or
        green < 0 or green > 100 or
        blue < 0 or blue > 100):
      return
    self._lock.acquire()
    self._rgb.set_rgb(red, green, blue)
    self._publish_message(
        'rgb/status',
        ','.join([
            int(red / 100.0 * 255),
            int(green / 100.0 * 255),
            int(blue / 100.0 * 255)]))
    self._lock.release()

  def get_rgb(self):
    """Gets the red, green, and blue values.

    Returns:
      Value of the red, green, and blue LED pin PWM duty cycles as a tuple.
    """
    self._lock.acquire()
    self._rgb.get_rgb()
    self._lock.release()

  def set_red(self, value):
    """Sets the red value.

    Args:
      value: Integer, 0-100. Value for red led.
    """
    if red < 0 or red > 100:
      return
    self._lock.acquire()
    self._rgb.set_red(value)
    self._publish_message(
        'rgb/status',
        ','.join([
            int(self._rgb.get_red() / 100.0 * 255),
            int(self._rgb.get_green() / 100.0 * 255),
            int(self._rgb.get_blue() / 100.0 * 255)]))
    self._lock.release()

  def get_red(self):
    """Gets the red value.

    Returns:
      Value of the red LED pin PWM duty cycle.
    """
    self._lock.acquire()
    val = self._rgb.get_red()
    self._lock.release()
    return val

  def set_green(self, value):
    """Sets the green value.

    Args:
      value: Integer, 0-100. Value for green led.
    """
    if green < 0 or green > 100:
      return
    self._lock.acquire()
    self._rgb.set_green(value)
    self._publish_message(
        'rgb/status',
        ','.join([
            int(self._rgb.get_red() / 100.0 * 255),
            int(self._rgb.get_green() / 100.0 * 255),
            int(self._rgb.get_blue() / 100.0 * 255)]))
    self._lock.release()

  def get_green(self):
    """Gets the green value.

    Returns:
      Value of the green LED pin PWM duty cycle.
    """
    self._lock.acquire()
    val = self._rgb.get_green()
    self._lock.release()
    return val

  def set_blue(self, value):
    """Sets the blue value.

    Args:
      value: Integer, 0-100. Value for blue led.
    """
    if blue < 0 or blue > 100:
      return
    self._lock.acquire()
    self._rgb.set_blue(value)
    self._publish_message(
        'rgb/status',
        ','.join([
            int(self._rgb.get_red() / 100.0 * 255),
            int(self._rgb.get_green() / 100.0 * 255),
            int(self._rgb.get_blue() / 100.0 * 255)]))
    self._lock.release()

  def get_blue(self):
    """Gets the blue value.

    Returns:
      Value of the blue LED pin PWM duty cycle.
    """
    self._lock.acquire()
    val = self._rgb.get_blue()
    self._lock.release()
    return val

  def fade(self, red, green, blue, delay_ms=500, step=5):
    """Fades to the specified red, green, blue values.

    Args:
      red: Integer, 0-100. Value for red LED.
      green: Integer, 0-100. Value for green LED.
      blue: Integer, 0-100. Value for blue LED.
      delay_ms: Time to stay at each step.
      step: Size of each step towards new value.
    """
    if (red < 0 or red > 100 or
        green < 0 or green > 100 or
        blue < 0 or blue > 100):
      return
    self._lock.acquire()
    self._rgb.fade(red, green, blue, delay_ms=delay_ms, step=step)
    self._lock.release()

  def enable(self):
    """Enables the LEDs."""
    self._lock.acquire()
    if not self._rgb.is_enabled():
      self._rgb.enable()
      self._publish_message('status/status', 'ON')
    self._lock.release()

  def disable(self):
    """Disables the LEDs."""
    self._lock.acquire()
    if self._rgb.is_enabled():
      self._rgb.disable()
      self._publish_message('status/status', 'OFF')
    self._lock.release()

  def serve_forever(self):
    self._client.connect(_MQTT_HOST)
    self._started = True
    self._client.loop_forever()


def main():
  platform = rpi_platform.RaspberryPiPlatform(_PIN_NAMING_SCHEME)

  red_pwm = platform.get_pwm_output(_RED_PIN)
  green_pwm = platform.get_pwm_output(_GREEN_PIN)
  blue_pwm = platform.get_pwm_output(_BLUE_PIN)
  rgb = rgb_led.RGBLed(red_pwm, green_pwm, blue_pwm)
  server = LightServer(rgb)

  server.set_pwm_frequency(_PWM_FREQUENCY_HZ)
  server.set_rgb(0, 0, 0)
  server.enable()

  server.serve_forever()


if __name__ == '__main__':
  main()

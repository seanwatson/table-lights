import nfldb
import time
import xmlrpclib

LIGHT_SERVER_HOST = '192.168.0.117:8888'
YEAR = 2015
WEEK = 6

REFRESH_DELAY = 60
BLINK_DELAY = 1

TEAM_COLORS = {
    'ARI': (60, 15, 26),
    'ATL': (74, 5, 9),
    'BAL': (16, 1, 33),
    'BUF': (0, 20, 55),
    'CAR': (0, 53, 80),
    'CHI': (1, 12, 18),
    'CIN': (98, 31, 8),
    'CLE': (15, 12, 12),
    'DAL': (5, 14, 30),
    'DEN': (98, 31, 8),
    'DET': (0, 43, 69),
    'GB': (12, 22, 19),
    'HOU': (1, 14, 23),
    'IND': (0, 23, 48),
    'JAC': (0, 40, 46),
    'KC': (70, 0, 50),
    'MIA': (0, 55, 59),
    'MIN': (23, 0, 38),
    'NE': (5, 14, 30),
    'NO': (82, 72, 53),
    'NYG': (10, 18, 42),
    'NYJ': (5, 22, 11),
    'OAK': (77, 78, 80),
    'PHI': (0, 23, 28),
    'PIT': (100, 71, 7),
    'SD': (0, 45, 81),
    'SEA': (41, 74, 16),
    'SF': (67, 12, 17),
    'STL': (7, 15, 29),
    'TB': (84, 4, 4),
    'TEN': (39, 56, 80),
    'WAS': (47, 19, 26),
}


def flash_lights(light_server, team, diff):
    print team, '+' + str(diff)
    for _ in range(diff):
        light_server.set_rgb(*TEAM_COLORS[team])
        time.sleep(BLINK_DELAY)
        light_server.set_rgb(0, 0, 0)
        time.sleep(BLINK_DELAY)


def main():
    light_server = xmlrpclib.ServerProxy('http://' + LIGHT_SERVER_HOST)
    db = nfldb.connect()
    games = {}
    print 'Starting nfl-alarm with light server', LIGHT_SERVER_HOST, 'for week', str(WEEK)
    while True:
        q = nfldb.Query(db)
        q.game(season_year=YEAR, week=WEEK, season_type='Regular')
        for g in q.as_games():
            if g.gsis_id not in games:
                games[g.gsis_id] = {
                    'home_team': g.home_team,
                    'home_score': g.home_score,
                    'away_team': g.away_team,
                    'away_score': g.away_score
                }
            else:
                diff = g.home_score - games[g.gsis_id]['home_score']
                if diff > 0:
                    games[g.gsis_id]['home_score'] = g.home_score
                    flash_lights(light_server, g.home_team, diff)
                diff = g.away_score - games[g.gsis_id]['away_score']
                if diff > 0:
                    games[g.gsis_id]['away_score'] = g.away_score
                    flash_lights(light_server, g.away_team, diff)
        time.sleep(REFRESH_DELAY)


if __name__ == '__main__':
    main()
